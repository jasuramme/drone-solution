#!/usr/bin/env python

import time
from math import sin, cos
import numpy as np

import copy
import rospy
import math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image

from hector_uav_msgs.srv import EnableMotors

import cv2
from cv_bridge import CvBridge, CvBridgeError

from nav_msgs.msg import Odometry
import tf2_ros
import tf.transformations as ttt

PLANING_HORIZON = 50

TIME_LIFTOFF = 3

V_MAX = 2.05
W_MAX = 0.35

Kp_z = 0.5

Kp_y =  0.015
Kd_y =  0.000045
Ki_y =  0.0000825

Kp_w =  0.01555
Kd_w =  0.000095
Ki_w =  0.000165

class Pid():

    def __init__(self, goal, K, satruation, no_react, debug=0):
        self.x = 0.0
        self.dx = 0.0
        self.i = 0.0
        self.goal = goal
        self.K = K
        self.error = 0.0
        self.out = 0.0
        self.satruation = satruation
        self.debug = debug
        self.no_react = no_react

    def set_value(self, val):
        self.dx = val - self.x
        self.x = val

    def set_error(self, err):
        self.x = err
        self.goal = 0.0
        self.dx = err - self.error
        self.error = err

    def get_out(self):
        self.error = self.goal - self.x
        self.i += self.error
        K = self.K
        self.out = K[0] * self.error + K[1] * self.dx + K[2] * self.i
        if self.satruation:
            if self.out > self.satruation:
                self.out = self.satruation
            if self.out < -self.satruation:
                self.out = -self.satruation
        if self.no_react and abs(self.error) < abs(self.no_react):
            self.out = 0.0
        if self.debug > 0:
            print('[err:', self.error, '|dx:', self.dx, '|i:', \
                self.i, '|out', self.out, ']')
        if self.debug > 1:
            print('[fx:', self.error * K[0], '|fdx:', self.dx * K[1], '|fi:', \
                self.i * K[2], ']')
        return self.out


class SimpleMover():

    def __init__(self):
        rospy.init_node('line_follower', anonymous=True)
        self.image = []
        self.image_front = []
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        
        self.cv_bridge = CvBridge()

        rospy.on_shutdown(self.shutdown)

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        self.drone_state = [0] * 6  # position vector
        self.drone_prev_state = [0] * 6
        self.e_y = 0
        self.e_omega_z = 0
        self.rate = rospy.Rate(30)
        self.omega_error = 0.0
        self.vel_pid = Pid(1.5, [1.0, -1.2, 0.001], None, None, 0)
        self.z_pid = Pid(1.8, [5.0, -120, 0.1], None, None, 0)
        self.omega_pid = Pid(0.0, [0.1, -0.01, 0.0], None, None, 0)
        self.omega_y_pid = Pid(0.0, [-0.004, 0.001, 0.0], None, None, 0)
        self.y_pid = Pid(0.0, [-0.01, 0.0, 0.0], None, 50.0, 0)
        self.last_bigerr = rospy.get_time()
        self.current_vel = 0.0
        self.updated = rospy.get_time()
        self.y_error = 0.0
        self.blue_last_seen = rospy.get_time()
        self.y_error_prev = 0.0
        self.blue_ring_detected = False
        self.red_ring_detected = False
        self.zadd = 0.0
        self.uptime = 0.0
        self.slowed_down = False
        self.blur = [21,91]
        self.lookUpTable = np.empty((256), np.uint8)
        self.decrease_speed = 0.0
        for i in range(256):
            if i < 245:
                self.lookUpTable[i] = 0
            else:
                self.lookUpTable[i] = 255

        rospy.Subscriber("cam_1/camera/image", Image, self.camera_callback)
        rospy.Subscriber("cam_2/camera/image", Image, self.camera_front_callback)
        rospy.Subscriber('/ground_truth/state', Odometry, self.obom_callback)


    def obom_callback(self, msg):
        """ Pose of a robot extraction"""
        #self.listener.waitForTransform('/world', '/base_stabilized', rospy.Time(), rospy.Duration(4.0))
        success = False
        while not success:
            try:
                transform = self.tfBuffer.lookup_transform('world', 'base_stabilized', rospy.Time()).transform
                x, y, z = transform.translation.x, transform.translation.y, transform.translation.z
                quat = transform.rotation
                r, p, y = ttt.euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])
                self.drone_prev_state = copy.deepcopy(self.drone_state)
                self.drone_state = [x, y, z, r, p, y]
                dx = self.drone_state[0] - self.drone_prev_state[0]
                dy = self.drone_state[1] - self.drone_prev_state[1]
                self.current_vel = math.sqrt(dx*dx + dy*dy)
                self.vel_pid.set_value(self.current_vel)
                self.z_pid.set_value(self.drone_state[2])
                self.omega_pid.dx = dx = self.drone_state[5] - self.drone_prev_state[5]
                success = True
            except:
                self.rate.sleep()
                print("Error getting frame")
                return

    def camera_front_callback(self, msg):
        try:
           orig_image = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
           cv_image = orig_image
        except CvBridgeError as e:
           rospy.logerr("CvBridge Error: {0}".format(e))
 
        # red
        lower = np.uint8([0, 0, 90])
        upper = np.uint8([30, 30, 120])
        cv_image, red_pose, red_radius  = self.ring_detector(orig_image, lower, upper, (0,0,255))
    
        # blue
        lower = np.uint8([40, 20, 20])
        upper = np.uint8([80, 50, 50])
        cv_image, blue_pose, blue_radius = self.ring_detector(cv_image, lower, upper, (255,0,0))
    
        # print(red_radius, blue_radius)
    
        if 70 < red_radius < 90 or 55 < blue_radius < 90:
            if red_radius > blue_radius:
                self.blue_ring_detected = False
                self.red_ring_detected = True
            else:
                self.red_ring_detected = False
                self.blue_ring_detected = True
                
                # offset in ring xy-plane to fly through center of a ring
                # error = <center of image> - <center of ring>
                self.e_x_blue = 160 - blue_pose[0]
                self.e_y_blue = 120 - blue_pose[1]
        else:
            self.blue_ring_detected = False
            self.red_ring_detected = False
    
        # save results
        self.image_front = cv_image

        #self.show_image(cv_image_front, title='rings')


    def camera_callback(self, msg):
        """ Computer vision stuff"""
        try:
            cv_image = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))
            return

        grey_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(grey_image, 8, 255, cv2.THRESH_BINARY_INV)
        mask = cv2.GaussianBlur(mask, (self.blur[0], self.blur[1]), 0)
        mask = cv2.LUT(mask, self.lookUpTable)
        #mask = cv2.Canny(mask, 150, 200, apertureSize=5)
        cv_image = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

        cv2.line(cv_image, (160, 0), (160, 240), (0, 123, 0), 1)
        cv2.line(cv_image, (0, 120), (320, 120), (0, 123, 0), 1)

        # "steering" conrol
        top_points = np.where(mask[10] >= 10)
        mid_points = np.where(mask[msg.height / 2] >= 10)
        if  (not np.isnan(np.average(top_points)) and not np.isnan(np.average(mid_points))):
            top_line_point = int(np.average(top_points))
            mid_line_point = int(np.average(mid_points))
            self.omega_error = top_line_point - mid_line_point

            cv2.circle(cv_image, (top_line_point, 10), 5, (0,0,255), 1)
            cv2.circle(cv_image, (mid_line_point, int(msg.height/2)), 5, (0,0,255), 1)
            cv2.line(cv_image, (mid_line_point, int(msg.height/2)), (top_line_point, 10), (0, 0, 255), 3)

        # y-offset control
        __, cy_list = np.where(mask >= 10)
        if not np.isnan(np.average(cy_list)):
            cy = int(np.average(cy_list))
            self.y_error = msg.width / 2 - cy
            
            cv2.circle(cv_image, (cy, int(msg.height/2)), 7, (0,255,0), 1)
            cv2.line(cv_image, (160, 120), (cy, int(msg.height/2)), (0, 255, 0), 3)

        self.omega_pid.set_error(self.omega_error)
        self.y_pid.set_error(self.y_error)
        self.omega_y_pid.set_error(self.y_error)
        self.image = cv_image
        #self.show_image(cv_image, title='line')


    def show_image(self, img, title='Camera 1'):
        cv2.imshow(title, img)
        cv2.waitKey(3)

    def enable_motors(self):
        try:
            rospy.wait_for_service('enable_motors', 2)
            call_service = rospy.ServiceProxy('enable_motors', EnableMotors)
            response = call_service(True)
        except Exception as e:
            print("Error while try to enable motors: ", e)

    def pd(self, err, dx, kp = 1.0, kd = -1.0):
        return kp * (err) + kd * dx

    def ring_detector(self, image, lower, upper, color):
        color_mask = cv2.inRange(image, lower, upper)
        color_contours, _ = cv2.findContours(color_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        if color_contours:
            max_len_c = 0
            c = color_contours[0]
            for i in range(0, len(color_contours)):
                if len(color_contours[i]) > max_len_c:
                    c = color_contours[i]
                    max_len_c = len(color_contours[i])
            self.color_distance = max_len_c
            M = cv2.moments(c)
            if M['m00'] != 0:
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])
            else:
                cx = 0
                cy = 0
            (x1,y1), color_r = cv2.minEnclosingCircle(c)
            if color_r > 10:
                image = cv2.circle(image, (cx, cy), radius=5, color=color, thickness=-1)
                cv2.drawContours(color_r, c, -1, (0,255,0), 1)
                color_r = cv2.circle(color_r, (int(x1), int(y1)), radius=int(color_r), color=color, thickness=4)       
                return image, (x1,y1), color_r[0]
        return image, (0,0), 0
       
    """def update_speed(self, angle, offset):
        angle = abs(angle)
        offset = abs(offset)
        if rospy.get_time() - self.updated > 0.05: 
            self.updated = rospy.get_time()
            if angle >= 30.0:
                self.vel_des -= 0.1
            else:
                self.vel_des += 0.04
            if offset >= 50.0:
                self.vel_des -= 0.2
        if self.vel_des >= self.vel_max:
            self.vel_des = self.vel_max
        if self.vel_des <= self.vel_min:
            self.vel_des = self.vel_min
        print(angle, ' -- ', offset, ' -- ', self.vel_des)
        return self.vel_des"""

    def spin(self):
        self.enable_motors()
        
        # Initialisations
        altitude_prev = 0
        y_error_prev = 0
        omega_error_prev = 0

        alpha = self.drone_state[5]

        time_start = rospy.get_time()
        time_prev = time_start
        while not rospy.is_shutdown():
            try:
                # Time stuff
                t = rospy.get_time() - time_start
                dt = t - time_prev
                time_prev = t
                if dt == 0:
                    dt = 1 / 30.

                h = 2.3
                jumping = False
                if self.blue_ring_detected:
                    self.blue_last_seen = rospy.get_time()
                    self.zadd = h + 1.0
                    jumping = True
                    if self.slowed_down:
                        self.uptime = 4.0
                    else:
                        self.uptime = 0.5
                elif rospy.get_time() - self.blue_last_seen > self.uptime:
                    self.zadd = h
                if self.red_ring_detected:
                    self.zadd = h

                self.z_pid.goal = self.zadd

                if len(self.image) > 0 and len(self.image_front) > 0:
                    self.show_image(self.image, title='Line')
                    self.show_image(self.image_front, title='Rings')
            
                u_x = self.vel_pid.get_out()
                u_z = self.z_pid.get_out()
                u_omega = self.omega_pid.get_out()
                u_omega_y = self.omega_y_pid.get_out()
                u_y = self.y_pid.get_out()
                ang = u_omega + u_y

                u_x = 3.5
                dec = 0.3
                acc = 0.001
                dmax = 1.5
                bigerr = 100

                if abs(self.y_error) > bigerr and not jumping:
                    self.last_bigerr = rospy.get_time()
                    self.decrease_speed = min( self.decrease_speed + dec, dmax)
                    self.slowed_down = True
                elif abs(self.y_error) < bigerr and \
                    rospy.get_time() - self.blue_last_seen > 2.0:
                    self.decrease_speed -= max (self.decrease_speed + acc, 0.0)
                
                if self.decrease_speed <= dec:
                    self.slowed_down = False
                u_x = u_x - self.decrease_speed
                print(int(self.omega_error),'__==__',int(self.y_error),':',u_x,':',self.decrease_speed)
                twist_msg = Twist()
                twist_msg.linear.x = u_x
                twist_msg.linear.y = u_y
                twist_msg.linear.z =  u_z
                twist_msg.angular.z = u_omega + u_omega_y
                self.cmd_vel_pub.publish(twist_msg)

            except KeyboardInterrupt:
                break

            self.rate.sleep()

    def shutdown(self):
        self.cmd_vel_pub.publish(Twist())
        rospy.sleep(1)


if __name__=="__main__":
    simple_mover = SimpleMover()
    simple_mover.spin()
