#!/usr/bin/env python

import time

import rospy
from geometry_msgs.msg import Twist

from hector_uav_msgs.srv import EnableMotors


class SimpleMover():

    def __init__(self):
        print("Initializing")
        rospy.init_node('simple_mover', anonymous=True)
        self.rate = rospy.Rate(30)
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        rospy.on_shutdown(self.shutdown)

    def enable_motors(self):
        # (2) Call here the ros-service 'enable_motors'
        # TODO write code here
        print("Wait for service")
        rospy.wait_for_service('enable_motors')
        print("Service found")
        try:
            enable_motors = rospy.ServiceProxy('enable_motors', EnableMotors)
            enable_motors(True)
            print("Motors enabled")
        except rospy.ServiceException as e:
            print("Failed to start motors: %s"%e)

    def take_off(self):
        self.enable_motors()
        start_time = time.time()
        end_time = start_time + 3
        print("Taikng off")
        while time.time() < end_time:
            t = Twist()
            t.linear.x = 0.0
            t.linear.y = 0.0
            t.linear.z = 3.0
            self.cmd_vel_pub.publish(t)
            self.rate.sleep()

    def spin(self):

        self.take_off()
        print("GO AHEAD!")

        while not rospy.is_shutdown():
            t = Twist()
            t.linear.x = 0.5
            t.linear.y = 0.0
            t.linear.z = 0.0
            self.cmd_vel_pub.publish(t)
            self.rate.sleep()
            

    def shutdown(self):
        self.cmd_vel_pub.publish(Twist())
        rospy.sleep(1)


if __name__=="__main__":
    simple_mover = SimpleMover()
    simple_mover.spin()
